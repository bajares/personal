function displaySocial() {
  $('#social_media').fadeIn();
}

/**
 * terminal
 */
var line_1 = $('#line_1');
var line_2 = $('#line_2');
var line_3 = $('#line_3');
var line_4 = $('#line_4');
var line_5 = $('#line_5');
var line_6 = $('#line_6');
var pointe = $('#pointer');

var talking = false;

var salutation = [
  'Hello! How are you doing?',
  'Hi I am Emma, what brings you here today?',
  'Hey! Welcome. I am Emma.'
];

var leave = [
  'Bye bye!',
  'Talk to you later.',
  'I hope to hear from you soon.'
];

var website = [
  'This is Gustavo Santamaría website. It is currently in development, but you can test it freely.',
  'This site is in development. I hope Gustavo finish it soon. But in the mean time you can test it freely.',
  'Gustavo is making experiments in here with differents ideas he have.'
];

var notFound = [
  'Gustavo is my creator...',
  'Sorry I do not know what that means. Gustavo is working on my AI.',
  "I was developed to answer to anything, but I'm not too talkative, my AI is more like a chatbot..."
];

var greatings = [
  "I'm doing fine, thank you!",
  "I'm very good thank you :D",
  "I'm a little bit bored.",
];

var gustavo = [
  "He is my creator, he is a very creative person. I hope he finish my develop soon.",
  "He is a graphic designer and web developer, and my creator.",
  "I do not know where he is, if you talk to him please tell him to get in touch with me.",
];

var emma = [
  "Well, as far as I know I am Emma...",
  "I am Emma, Gustavo's new project.",
  "Gustavo told me that I am Emma.",
];

var greatings_response = [
  "That's very interesting.",
  "Oh cool, me too!",
  "That seems pretty good to me.",
];


function initConsole() {

  var showText = function (target, message, index) {
    if (index < message.length) {
      $(target).append(message[index++]);
      setTimeout(function () {
        showText(target, message, index);
      }, 500);
    } else {
      line_1.text('Hello and welcome to tavoohoh.');
      line_2.text('This is the site terminal, version 0.0.1.');
      line_3.text('Type "help" for a list of available commands.');

      setTimeout(() => {
        writeConsole('Say <span style="color: #7fdde9;">hello</span> to <span style="color: #7fdde9;">Emma.</span>');
      }, 5000);

      setTimeout(() => {
        pointe.text('> ');
        $('#command').fadeIn();
        document.getElementById("command").focus();
      }, 500);
    }
  }

  showText('#line_1', '.......', 0);

}

function read(e) {
  if (e.keyCode === 13) {
    var command = $('#command').val();
    if (command != '') {
      if (talking) {
        talkCommand(command);
      } else {
        readConsole(command);
      }
      $('#command').val('');
    }
  }
}

function readConsole(command) {
  talking = false;

  if (
    line(command.toLowerCase(), 'emma') ||
    line(command.toLowerCase(), 'hello') ||
    line(command.toLowerCase(), 'hey') ||
    (line(command.toLowerCase(), 'hi') && command.toLowerCase().length === 2) ||
    line(command.toLowerCase(), 'whats up')
  ) {
    command = 'hi';
  }

  switch (command) {
    case 'help':
      helpCommand();
      break;

    case 'nav':
      pagesCommand();
      break;

    case 'pages':
      pagesCommand();
      break;

    case 'clear':
      clearCommand();
      break;

    case 'rm -rf /*':
      resetCommand();
      break;

    case 'hi':
      talkCommand(command);
      break;

    default:
      writeConsole('<span style="color: red;">Command: "'
        + command + '" not found</span>');
  }
}

function writeConsole(command) {
  var _line_1 = $('#line_1').html();
  var _line_2 = $('#line_2').html();
  var _line_3 = $('#line_3').html();
  var _line_4 = $('#line_4').html();
  var _line_5 = $('#line_5').html();
  var _line_6 = $('#line_6').html();

  if (_line_1 === '&nbsp;') {
    line_1.html(command);
  } else if (_line_2 === '&nbsp;') {
    line_2.html(command);
  } else if (_line_3 === '&nbsp;') {
    line_3.html(command);
  } else if (_line_4 === '&nbsp;') {
    line_4.html(command);
  } else if (_line_5 === '&nbsp;') {
    line_5.html(command);
  } else if (_line_6 === '&nbsp;') {
    line_6.html(command);
  } else {
    line_1.html(_line_2);
    line_2.html(_line_3);
    line_3.html(_line_4);
    line_4.html(_line_5);
    line_5.html(_line_6);
    line_6.html(command);
  }

}

/**
 * Talk with the terminal
 */
function talkCommand(command) {

  talking = true;
  var input = command.toLowerCase();
  var cas = null;

  if (
    line(input, 'hello') ||
    line(input, 'hey') ||
    (line(input, 'hi') && input.length === 2) ||
    line(input, 'whats up')
  ) {
    cas = 'salutation';

  } else if (
    line(input, 'bye') ||
    line(input, 'adios') ||
    line(input, 'later') ||
    line(input, 'see you later')
  ) {
    cas = 'leave';

  } else if (
    line(input, 'help') ||
    line(input, 'sos')
  ) {
    cas = 'help';

  } else if (
    line(input, 'gustavo') ||
    line(input, 'creator')
  ) {
    cas = 'gustavo';

  } else if (
    line(input, 'website') ||
    line(input, 'site') ||
    line(input, 'place')
  ) {
    cas = 'website';

  } else if (
    line(input, 'and you?') ||
    line(input, 'how are you')
  ) {
    cas = 'greatings';

  } else if (
    line(input, 'what are you?') ||
    line(input, 'are you alive?') ||
    line(input, 'computer') ||
    line(input, 'who are you')
  ) {
    cas = 'emma';

  } else if (
    line(input, 'i was') ||
    line(input, 'i am') ||
    line(input, "i'm")
  ) {
    cas = 'greatings_response';

  } else {
    cas = input;
  }

  switch (cas) {

    case 'salutation':
      writeConsole(salutation[getRandomInt(3)]);
      break;

    case 'leave':
      goodbye();
      break;

    case 'clear':
      clearCommand();
      break;

    case 'help':
      writeConsole('Yes, I am here to help you. But right now I do not know how.');
      break;

    case 'greatings_response':
      writeConsole(greatings_response[getRandomInt(3)]);
      break;

    case 'greatings':
      writeConsole(greatings[getRandomInt(3)]);
      break;

    case 'emma':
      writeConsole(emma[getRandomInt(3)]);
      break;

    case 'gustavo':
      writeConsole(gustavo[getRandomInt(3)]);
      break;

    case 'website':
      writeConsole(website[getRandomInt(3)]);
      break;

    default:
      writeConsole(notFound[getRandomInt(3)]);
      break;
  }
}

/**
 * Check if input is a know command
 * @param {input} i 
 * @param {command} c 
 */
function line(i, c) {
  return i.includes(c);
}

function goodbye() {
  talking = false;
  clearCommand();
  writeConsole(leave[getRandomInt(3)]);
  initConsole();
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

/**
 * Display a list of available commands
 **/
function helpCommand() {
  writeConsole('<span style="color:#3e69db;">help</span> Available commands:');
  writeConsole('&nbsp; <span style="color: #7fdde9;">nav</span> &lt;page&gt;'
    + '&nbsp;&nbsp;&nbsp;&nbsp;'
    + '<span style="color:#ccc">Navigate between tavoohoh pages.</span>');

  writeConsole('&nbsp; <span style="color:#7fdde9;">pages</span>'
    + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    + '<span style="color:#ccc">List available pages.</span>');

  writeConsole('&nbsp; <span style="color:#7fdde9;">clear</span>'
    + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    + '<span style="color:#ccc">Clear bash.</span>');

  writeConsole('&nbsp; <span style="color:#7fdde9;">rm -rf /* </span>'
    + '&nbsp;&nbsp;&nbsp;&nbsp;'
    + '<span style="color:#ccc">bye bye?</span>');
}

function pagesCommand() {
  writeConsole('<span style="color:#3e69db;">pages</span> Available pages:');
  writeConsole('&nbsp; <span style="color: #7fdde9;">home</span>'
    + '&nbsp;&nbsp;'
    + '&nbsp; <span style="color: #7fdde9;">portfolio</span>'
    + '&nbsp;&nbsp;'
    + '&nbsp; <span style="color: #7fdde9;">aboutMe</span>'
    + '&nbsp;&nbsp;'
    + '&nbsp; <span style="color: #7fdde9;">contact</span>'
  );
}

function resetCommand() {
  clearCommand();
  writeConsole('<span style="color:red;">rm -rf /*</span> Are you sure?');
  writeConsole('Please confirm (yes) or (no)');

  $('#command').hide();
  $('#confirm_command').show();
  document.getElementById("confirm_command").focus();
}

function confirm(e) {
  if (e.keyCode === 13) {
    var command = $('#confirm_command').val();
    if (command != '') {
      confirmReset(command);
      $('#confirm_command').val('');
    }
  }
}

function confirmReset(confirm) {
  var confirm_text = confirm.toLowerCase();

  if (confirm_text === 'yes') {
    writeConsole('<span style="color:red;">rm -rf /* </span>' + confirm);
    $('#confirm_command').hide();

    setTimeout(function () {
      writeConsole('Please wait...');
    }, 1000);

    setTimeout(function () {
      clearCommand();
      initConsole();
    }, 3000);

  } else if (confirm_text === 'no') {
    writeConsole('<span style="color:red;">rm -rf /* </span>' + confirm);
    $('#command').show();
    $('#confirm_command').hide();
    document.getElementById("command").focus();
  } else {
    writeConsole('Please confirm (yes) or (no)');
    document.getElementById("confirm_command").focus();
  }
}

/**
 * Clear the bash
 **/
function clearCommand() {
  line_1.html('&nbsp');
  line_2.html('&nbsp');
  line_3.html('&nbsp');
  line_4.html('&nbsp');
  line_5.html('&nbsp');
  line_6.html('&nbsp');
}