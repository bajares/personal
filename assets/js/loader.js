/**
 * Logo animation
 */

$(window).on('load', function () {
  setTimeout(() => {
    $('#loader').fadeOut(function () {
      $('#content').fadeIn().css("display", "grid")
      $('#header').fadeIn();
      $('#footer').fadeIn();
      initConsole();
    });
  }, 4000);
});

var animeEll = anime({
  targets: '#ell',
  top: ['-1000%', '0'],
  left: ['1000%', '0'],
  easing: 'easeInOutQuart',
  delay: 300
});

var animeSqu = anime({
  targets: '#squ',
  right: ['-1000%', '0'],
  easing: 'easeInOutQuart',
  delay: 300
});

var animeTri = anime({
  targets: '#tri',
  bottom: ['-1000%', '0'],
  left: ['-1000%', '0'],
  easing: 'easeInOutQuart',
  delay: 300
});

var animeTxt = anime({
  targets: ['#txt', '.main-footer'],
  opacity: ['0', '1'],
  duration: 4000,
  easing: 'easeInOutQuart',
  delay: 1000
});
